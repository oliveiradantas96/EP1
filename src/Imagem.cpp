#include "Imagem.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>

using namespace std;


Imagem::Imagem() {

}

Imagem::Imagem(string nome_imagem) {
	this->nome_imagem = nome_imagem;
	
}

string Imagem::getNome() {
	return nome_imagem;
}

void Imagem::setNome(string nome_imagem) {
	this->nome_imagem = nome_imagem;
}

string Imagem::getTipo(){
	return tipo;

}
void Imagem::setTipo(string tipo){
	this->tipo = tipo;
}

string Imagem::getComentario(){
	return comentario;

}
void Imagem::setComentario(string comentario){
	this->comentario = comentario;
}

int Imagem::getAltura(){
	return altura;

}
void Imagem::setAltura(int altura){
	this->altura = altura;
}
int Imagem::getLargura(){
	return largura;

}
void Imagem::setLargura(int largura){
	this->largura = largura;
}

int Imagem::getIntensidade(){
	return intensidade;

}
void Imagem::setIntensidade(int intensidade){
	this->intensidade = intensidade;
}





void Imagem::grava_imagem() { //conferir e gravar a imagem

	ifstream arquivo_entrada;
	ofstream arquivo_saida;

	string tipo;
	string comentario;
	int altura;
	int largura;
	int intensidade;

	arquivo_entrada.open(("./doc/" + nome_imagem + ".ppm").c_str());

	arquivo_entrada >> tipo;
	setTipo(tipo);

	arquivo_entrada >> comentario;
	setComentario(comentario);

	arquivo_entrada >> altura;
	setAltura(altura);

	arquivo_entrada >> largura;
	setLargura(largura);

	arquivo_entrada >> intensidade;
	setIntensidade(intensidade);
	
	if(tipo != "P6") {
		cout << "Desculpe, arquivo do formato inválido." << endl;
	}


	arquivo_entrada.close();
/*

arquivo.open ((getNome_imagem()) , ifstream::in);
	while(controle ==0){
		arquivo.getline(line,150);
		if(primeira_execucao == 0){
			if(strcmp(line, "P6") >  0){
				cout << "O arquivo nao esta no formato P6" << endl;
				arquivo.close();
				exit(1);
			}
			primeira_execucao++;

		}
		cout << line << endl;


		if(strcmp(line , "255") == 0){
			controle++;
		} 
	}
arquivo.close();
*/
/*
arquivo.open (pergunta, std::ifstream::binary);
arquivo2.open ("copia.ppm", std::ifstream::binary);

if(!arquivo.is_open() || !arquivo2.is_open())  //  verifica se o programa conseguiu abrir A IMAGEM
        {
        cout<<"Não foi possível abrir o arquivo!\n";
        arquivo.close(); 
        arquivo2.close();
	 	exit(1);
        }

	while(arquivo.good()){
		arquivo.read(&conteudo,sizeof(char));
		arquivo2.write(&conteudo,sizeof(char));

    }

    

    arquivo.close();
    arquivo2.close();
	exit(1);
*/
}