#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <iostream>
#include <string>

using namespace std;

class Imagem {
private: //Atributos

	string nome_imagem;
	string tipo; //P6
	string comentario;
	int altura;
	int largura;
	int intensidade;


public:
	Imagem();
	Imagem(string nome_imagem);
	
	string getNome();
	void setNome(string nome_imagem);

	string getTipo();
	void setTipo(string tipo);

	string getComentario();
	void setComentario(string comentario);

	int getAltura();
	void setAltura(int altura);

	int getLargura();
	void setLargura(int largura);

	int getIntensidade();
	void setIntensidade(int intensidade);
	

	void grava_imagem();
};


#endif
