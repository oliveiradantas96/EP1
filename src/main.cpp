#include "Imagem.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;


int main() {

	string pergunta;

	cout << "Qual Imagem você deseja abrir: ";
	cin >> pergunta;
	
	Imagem imagem(pergunta);
	imagem.grava_imagem();
	
	cout << "Nome: " << imagem.getNome() << endl;

	cout << "Tipo: " << imagem.getTipo() << endl;

	cout << "Comentário: " << imagem.getComentario() << endl;

	cout << "Altura: " << imagem.getAltura() << endl;

	cout << "Largura: " << imagem.getLargura() << endl;

	cout << "Intensidade: " << imagem.getIntensidade() << endl;



	



	return 0;
}