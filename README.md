# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

 
### Como Compilar e Executar
 
 
 Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:
 
 * Salve a imagem .ppm a ser lida na pasta /.Doc/ do Projeto;
 * Abra o terminal;
 * Encontre o diretório raiz do projeto;
 * Limpe os arquivos objeto:
 	**$ make clean** 
 * Compile o programa: 
 	**$ make**
 * Execute:
 	**$ make run**
 * Insira o nome da imagem a ser aberta, padrao de teste unb.ppm;
 * Insira o novo nome a ser salvo na mesma pasta da imagem escolhida.
